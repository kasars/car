/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License.
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */

#include <Servo.h>
#include "QBlueStickReader.h"

Servo servo;

const int angleAmpl = 30;
const int middleAngle = 90;

const int stickAmplMax = 127;
int       minAmpl = 20;


int oldSpeed = 0;
int oldPwm = 255;
int oldStickAngle = 0;

const int onOffLeftPin = 7;
const int onOffRightPin = 8;
const int pwmLeftPin = 3;
const int pwmRightPin = 5;

QBlueStickReader stick;


void customDataReceived(char inData)
{
  Serial.println(inData);
}

void setup()
{
    pinMode(onOffLeftPin, OUTPUT);
    pinMode(onOffRightPin, OUTPUT);
    pinMode(pwmLeftPin, OUTPUT);
    pinMode(pwmRightPin, OUTPUT);
    servo.attach(11);


    Serial.begin(9600);

    stick.setButtonText(0, "");
    stick.setButtonText(1, "");
    stick.setButtonText(2, "");
    stick.setButtonText(3, "");
    stick.setButtonText(4, "");
    stick.setButtonText(5, "");
    stick.setButtonText(6, "");
    stick.setButtonText(7, "");

    stick.setAxisState(0, QBlueStickReader::Disabled);
    stick.setAxisState(2, QBlueStickReader::Turn);
    stick.setAxisState(3, QBlueStickReader::Disabled);

    stick.setCustomDataCalback(&customDataReceived);
}

void serialEvent() {
    while (Serial.available()) {
        stick.parseChar((char)Serial.read());
    }
}

int stickToPwm(int ampl) {
  if (ampl < 0) {
    ampl = -ampl;
  }
  int diff = stickAmplMax - minAmpl;
  int scaled = (ampl * diff)/stickAmplMax + minAmpl;
  int pwm = (scaled * scaled)/63;
  if (pwm > 255) {
    pwm = 255;
  }
  return pwm;
}

void loop()
{
  unsigned long timeDiff = millis() - stick.lastMessageTime();

  int stickAngle = stick.axis3Value();
  if (timeDiff > 200) stickAngle = 0;

  if (stickAngle != oldStickAngle) {
    oldStickAngle = stickAngle;
    int angleDiff = (stickAngle * angleAmpl) / stickAmplMax;
    servo.write(middleAngle + angleDiff);
  }
  
  int stickSpeed = stick.axis2Value();
  if (timeDiff > 200) stickSpeed = 0;

  if (stickSpeed != oldSpeed) {
    oldSpeed = stickSpeed;
    int motorPwm = stickToPwm(stickSpeed);
    if (oldPwm < motorPwm) {
      oldSpeed = stickSpeed * stickAmplMax / stickSpeed;
      motorPwm = 255;
      //Serial.println("PWM: Max..: " + String(oldSpeed));
    }
    oldPwm = motorPwm;
    
    Serial.println("PWM: " + String(motorPwm) + " stickSpeed: " + String(stickSpeed));
  
    if (stickSpeed < 0) {
      digitalWrite(onOffRightPin, LOW);
      analogWrite(pwmLeftPin, 0);
      digitalWrite(onOffLeftPin, HIGH);
      analogWrite(pwmRightPin, motorPwm);    
    }
    else if (stickSpeed == 0) {
      digitalWrite(onOffRightPin, LOW);
      analogWrite(pwmLeftPin, 0);
      digitalWrite(onOffLeftPin, LOW);
      analogWrite(pwmRightPin, 0);    
    }
    else {
      digitalWrite(onOffLeftPin, LOW);
      analogWrite(pwmRightPin, 0);
      digitalWrite(onOffRightPin, HIGH);
      analogWrite(pwmLeftPin, motorPwm);    
    }
  }
  delay(10);
}


