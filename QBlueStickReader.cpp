/* ============================================================
 *
 * Copyright (C) 2018 by Kåre Särs <kare.sars@iki.fi>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA or see <http://www.gnu.org/licenses/>
 *
 * ============================================================ */

#include "QBlueStickReader.h"


QBlueStickReader::QBlueStickReader()
{
    for (int i=0; i<8; ++i) {
        setButtonText(i, String(i+1, DEC));
    }
    for (int i=0; i<4; ++i) {
        m_axisState[i] = Normal;
    }
}

bool QBlueStickReader::validButton(int button)
{
    if (button < 0 || button >= buttonCount()) {
        Serial.println("Bad button index: ");
        Serial.println(button, DEC);
        return false;
    }
    return true;
}

void QBlueStickReader::setButtonText(int button, const String &text)
{
    if (!validButton(button)) return;
    m_buttons[button].text = text;
}

void QBlueStickReader::setAxisState(int axis, AxisState state)
{
    if (axis < 0 || axis >= 4) {
        Serial.println("Bad axis index: ");
        Serial.println(axis, DEC);
        return;
    }
    m_axisState[axis] = state;
}


void QBlueStickReader::setCustomDataCalback(void (* calback)(char indata))
{
    m_calback = calback;
}

bool QBlueStickReader::buttonIsDown(int button)
{
    if (!validButton(button)) return false;

    return m_buttons[button].pressed;
}

bool QBlueStickReader::buttonToggledDown(int button)
{
    if (!validButton(button)) return false;

    bool toggledDown = m_buttons[button].pressed && !m_buttons[button].pressedOld;
    m_buttons[button].pressedOld = m_buttons[button].pressed;
    return toggledDown;
}

bool QBlueStickReader::buttonToggledUp(int button)
{
    if (!validButton(button)) return false;

    bool toggledUp = !m_buttons[button].pressed && m_buttons[button].pressedOld;
    m_buttons[button].pressedOld = m_buttons[button].pressed;
    return toggledUp;
}

bool QBlueStickReader::newInputData()
{
    bool newData = m_newInputData;
    m_newInputData = false;
    return newData;
}

static void printJsonStringPair(const String &text, const String &value, bool addComma)
{
    String pair = "\"" + text + "\": \"" + value + "\"";
    if (addComma) {
        pair = String(pair + ",");
    }
    pair = String(pair + " ");

    Serial.print(pair);
}

void QBlueStickReader::printConfig() {
    Serial.print("{ ");
    for (int i=0; i<buttonCount(); ++i) {
        printJsonStringPair("button" + String(i+1), m_buttons[i].text, true);
    }
    for (int i=0; i<4; ++i) {
        String mode;
        switch (m_axisState[i]) {
            case Normal:
                mode = "n";
                break;
            case Turn:
                mode = "t";
                break;
            case Disabled:
                mode = "d";
                break;
        }
        printJsonStringPair("axis" + String(i+1), mode, i < 3);
    }

    Serial.println("}");
}


void QBlueStickReader::parseChar(char inChar) {
    //Serial.print(m_readState, DEC);
    //Serial.print(" ");
    //Serial.println(inChar, DEC);
    switch(m_readState) {
        case FindHeader1:
            if (inChar == (char)0x80) {
                m_readState = FindHeader2;
            }
            else {
                Serial.print("Got: ");
                Serial.print(inChar, HEX);
                Serial.println(" Expected: 0x80");
            }
            break;
        case FindHeader2:
            if (inChar == 0)  m_readState = Command;
            else              m_readState = FindHeader1;
            break;
        case Command:
            if (inChar == CmdJoystick) {
                m_readState = ReadAxis1;
            }
            else if (inChar == CmdRequestSetup) {
                printConfig();
                m_readState = FindHeader1;
            }
            else if (inChar == CmdCustomData) {
                m_readState = ReadCustomData;
            }
            else {
                m_readState = FindHeader1;
            }
            break;
        case ReadAxis1:
            m_axis1 = inChar;
            m_readState = ReadAxis2;
            break;
        case ReadAxis2:
            m_axis2 = inChar;
            m_readState = ReadAxis3;
            break;
        case ReadAxis3:
            m_axis3 = inChar;
            m_readState = ReadAxis4;
            break;
        case ReadAxis4:
            m_axis4 = inChar;
            m_readState = ReadButtons;
            break;
        case ReadButtons:
            m_buttons[0].pressed = (inChar & 0x01) != 0;
            m_buttons[1].pressed = (inChar & 0x02) != 0;
            m_buttons[2].pressed = (inChar & 0x04) != 0;
            m_buttons[3].pressed = (inChar & 0x08) != 0;
            m_buttons[4].pressed = (inChar & 0x10) != 0;
            m_buttons[5].pressed = (inChar & 0x20) != 0;
            m_buttons[6].pressed = (inChar & 0x40) != 0;
            m_buttons[7].pressed = (inChar & 0x80) != 0;
            m_readState = FindHeader1;
            m_newInputData = true;
            break;
        case ReadCustomData:
            if (inChar == 0) {
                m_readState = FindHeader1;
            }
            else if (m_calback) {
                m_calback(inChar);
            }
            break;
    }

    m_lastMessageTime = millis();
}

void QBlueStickReader::printStatus() {
    Serial.print("A1: ");
    Serial.print(m_axis1, DEC);
    Serial.print(" A2: ");
    Serial.print(m_axis2, DEC);
    Serial.print(" A3: ");
    Serial.print(m_axis3, DEC);
    Serial.print(" A4: ");
    Serial.print(m_axis4, DEC);
    Serial.print(" Buttons: ");
    Serial.print(m_buttons[0].pressed, DEC);
    Serial.print(m_buttons[1].pressed, DEC);
    Serial.print(m_buttons[2].pressed, DEC);
    Serial.print(m_buttons[3].pressed, DEC);
    Serial.print(m_buttons[4].pressed, DEC);
    Serial.print(m_buttons[5].pressed, DEC);
    Serial.print(m_buttons[6].pressed, DEC);
    Serial.print(m_buttons[7].pressed, DEC);
    Serial.println(" ");
}

unsigned long QBlueStickReader::lastMessageTime() const
{
    return m_lastMessageTime;
}



